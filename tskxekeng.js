$(function() {
	$('#exercise-progress').progress();
	generateExercises(10);
	loadExercise();
});

let currentExercise = 0;

let verbs = [
	{
		"navi": "tul",
		"type": "vin",
		"english": "run"
	},
	{
		"navi": "ngop",
		"type": "vtr",
		"english": "create"
	},
	{
		"navi": "new",
		"type": "vtr",
		"english": "want"
	},
	{
		"navi": "slele",
		"type": "vin",
		"english": "swim"
	},
	{
		"navi": "taron",
		"type": "vtr",
		"english": "hunt"
	},
	{
		"navi": "kin",
		"type": "vtr",
		"english": "need"
	},
	{
		"navi": "kame",
		"type": "vtr",
		"english": "See"
	},
	{
		"navi": "tse'a",
		"type": "vtr",
		"english": "see"
	}
];

let nouns = [
	{
		"navi": "payoang",
		"english": "a fish"
	},
	{
		"navi": "Peyral",
		"english": "Peyral"
	},
	{
		"navi": "ioang",
		"english": "an animal"
	},
];

let exercises = [];

function generateExercises(count) {
	exercises.length = 0;
	for (let i = 0; i < count; i++) {
		exercises.push(generateExercise());
	}
}

function generateExercise() {
	let exercise = {};

	let verb = pickRandomFrom(verbs);
	let subject = pickRandomFrom(nouns);

	let object;
	if (verb.type === "vtr") {
		object = pickRandomFrom(nouns);
	}

	let q = subject.english + " " + ((subject.person === 1 || subject.person === 2) ? verb.english : conjugateEnglishVerb(verb.english))
	if (object) {
		q += " " + object.english;
	}

	q = q.charAt(0).toUpperCase() + q.slice(1);
	q += '.';
	exercise.question = q;

	let a = generateAgentive(subject.navi)[0] + " " + verb.navi;
	if (object) {
		a += " " + generatePatientive(object.navi)[0];
	}

	a = a.charAt(0).toUpperCase() + a.slice(1);
	a += '.';
	exercise.answer = a;

	if (object) {
		exercise.tree = {
			'type': 'any-order',
			'children': [
				{
					'type': 'agentive',
					'children': subject.navi
				},
				verb.navi,
				{
					'type': 'patientive',
					'children': object.navi
				}
			]
		};
	} else {
		exercise.tree = {
			'type': 'any-order',
			'children': [
				{
					'type': 'subjective',
					'children': subject.navi
				},
				verb.navi
			]
		};
	}

	return exercise;
}

// conjugates English verb to the -s form
function conjugateEnglishVerb(verb) {
	if (verb[verb.length - 1] === 'o') {
		return verb + 'es';
	} else {
		return verb + 's';
	}
}

function pickRandomFrom(array) {
	return array[Math.floor(Math.random() * array.length)];
}

function loadExercise() {
	let exercise = exercises[currentExercise];
	$('#question-text').html(exercise.question);
}

$('#answer-form').on('submit', function(e) {
	let exercise = exercises[currentExercise];
	let input = $('#answer-field').val();
	let isValid = checkValid(input, exercise.tree);
	$('#exercise-progress').progress('increment');
	if (isValid) {
		$('#correct-marker').show().fadeOut(3000);
		$('#exercise-progress').removeClass('red');
		$('#exercise-progress').addClass('green');
	} else {
		$('#incorrect-marker').show().fadeOut(3000);
		$('#error-modal').modal('show');
		$('#error-question').html(exercise.question);
		$('#error-your-answer').html(input);
		$('#error-correct-answer').html(exercise.answer);
		$('#exercise-progress').removeClass('green');
		$('#exercise-progress').addClass('red');
	}

	if (currentExercise + 1 == exercises.length) {
		$('#done-modal').modal('show');
		return false;
	}

	$('#answer-field').val('');

	// TODO wait until modal finished ...
	currentExercise++;
	loadExercise();
	return false;
});

function checkValid(sentence, tree) {
	let normalized = sentence
		.toLowerCase()
		.replace(/[.,\/#!$%\^&\*;:{}=\-_`~()]/g,"")  // remove punctuation
		.replace(/\s{2,}/g," ")  // remove double spaces
		.replace(/^\s+|\s+$/g, '');  // remove space on begin / end

	let options = generate(tree);
	console.log(options);
	return options.includes(normalized);
}

function generate(tree) {

	if (typeof(tree) === 'string') {
		return [tree.toLowerCase()];
	}

	let generateFunctions = {
		'any-order': generateAnyOrder,
		'or': generateOr,
		'subjective': generateSubjective,
		'agentive': generateAgentive,
		'patientive': generatePatientive
	};
	let result = generateFunctions[tree.type](tree.children);

	return result;
}

function generateAnyOrder(children) {
	if (children.length == 1) {
		return generate(children[0]);
	}

	let options = [];
	for (let i = 0; i < children.length; i++) { // for each last child
		let newChildren = children.slice(0);
		newChildren.splice(i, 1);
		let others = generateAnyOrder(newChildren);
		options = options.concat(cross(others, generate(children[i])));
	}

	return options;
}

// for ["a", "b"], ["c", "d"] produces ["a c", "a d", "b c", "b d"]
function cross(array1, array2) {
	let options = [];
	for (let i = 0; i < array1.length; i++) {
		for (let j = 0; j < array2.length; j++) {
			options.push(array1[i] + " " + array2[j]);
		}
	}
	return options;
}

function generateOr(children) {
	let options = [];
	for (let i = 0; i < children.length; i++) {
		options = options.concat(generate(children[i]));
	}
	return options;
}

function generateSubjective(noun) {
    return [noun]
}

function generateAgentive(noun) {
    if (endsInVowel(noun)) {
        return [noun + "l"]
    } else {
        return [noun + "ìl"]
    }
}

function generatePatientive(noun) {
    if (endsInVowel(noun)) {
        return [noun + "ti", noun + "t"]
    } else {
        if (endsInConsonant(noun)) {
            return [noun + "it", noun + "ti"]
        } else {
            if (noun.slice(-1) === "y") {
                if (noun.slice(-2) === "ey") {
                    return [noun + "t", noun + "ti"]
                } else {
                    return [noun + "it", noun + "t", noun + "ti"]
                }
            } else {
                return [noun + "it", noun + "ti"]
            }
        }
    }
}

function endsInVowel(noun) {
    var c = noun.slice(-1)
    return c === "a" || c === "ä" || c === "e" || c === "é" || c === "i" || c === "ì" || c === "o" || c === "u"
}

function endsInConsonant(noun) {
    var c = noun.slice(-2)
    return !endsInVowel(noun) && c !== "aw" && c !== "ay" && c !== "ew" && c !== "ey"
}

