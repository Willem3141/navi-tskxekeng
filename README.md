Tskxekeng - lì'fyari leNa'vi sä'o a tskxekeng sivi
==================================================

Fì'u lu sä'otsyìp a srung si ngaru, txo new pivlltxe nìNa'vi nìftue nì'ul. Sä'ol ngop aylì'uti 'Ìnglìsìyä, ulte pawm ngaru san rutxe ralpeng nìNa'vi sìk. Tsakrr pe'un ftxey ngeyä tìralpeng lu eyawr fuke. Nìsìlpey livu fìsä'o lesar ngaru.

Fte sivar, nìyey piak si pamrelur alu `index.html` fa Firefox fu Chrome (fu lahea vefyafa, slä tsunslu fwa ke zo tsakrr).

---

Tskxekeng - a tool to practice Na'vi
====================================

This is a small tool that helps you, if you want to speak in Na'vi more easily. The tool creates an English sentence and asks you to translate it into Na'vi. Then it decides whether your translation is correct or not. Hopefully this tool is useful to you.

To use it, simply open the file `index.html` using Firefox or Chrome (or using another method, but it might not work then).
